---
title: NIPS 2017 Tutorial on Fairness in Machine Learning
author: Solon Barocas, Moritz Hardt
---

<iframe src="https://player.vimeo.com/video/248490141" width="1200" height="675" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
<p style="font-size:10pt;"><a href="https://vimeo.com/248490141">GrandBallroom_Dec4_2_Fairness In Machine Leaning</a> from <a href="https://vimeo.com/user72337760">TechTalksTV</a> on <a href="https://vimeo.com">Vimeo</a>.</p>
<div style="width:1200px;text-align:center;border:1px solid
#ddd;background-color:#ff;">
<a style="font-size:32pt;" href="tutorial/index.html">Slides available here</a>
