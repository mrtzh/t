---
title: Fairness in machine learning
author: Solon Barocas, Moritz Hardt, Arvind Narayanan
date: May 5, 2018
---

## Available chapters

* [Introduction](introduction.html)
* [Prediction](prediction.html)

## Tutorials

* [Fairness in machine learning](tutorial1.html)
* [21 fairness definitions and their politics](tutorial2.html)

## Course materials

* [Berkeley CS 294: Fairness in machine learning](https://fairmlclass.github.io/)
