---
title: Prediction
---

Much work on fairness in machine learning focuses on *prediction* and
*regression*. Numerous fairness criteria have been proposed in this
context, although we will work out a compact taxonomy of different
approaches. There are fundamental trade-offs between the criteria we
examine. Strong limitations furthermore apply to a broad class of
classification criteria and hint at shortcomings of prediction paradigm.

Prediction setup
----------------

Consider jointy distributed random variables $X$ and $Y$. The goal of
prediction is to guess the value of $Y$ after observing the random
variable $X.$ The variable $X$ captures observed characteristics,
sometimes called *features*, from which we can try to predict the
unobserved *target* random variable $Y.$

We think of a *predictor* as a random variable $\hat Y$ in the same
probability space as $(X,Y).$ Predictors are typically learned from
historical data, a task referred to as *supervised learning*: Given
labeled examples drawn from the joint distribution of $(X,Y)$ come up
with a predictor for $Y$ given $X.$ When the target variable $Y$ takes
on two values, we speak of a binary prediction problem. In case the
target variable assumes multiple discrete values, we speak of
multi-class prediction.

In this section, we will focus on the statistical properties of the
random variables $(X,Y,\hat Y)$ without scrutinizing how $\hat Y$ was
learned, and what the functional relationship is between $X$ and
$\hat Y=f(X)$.

### Classification criteria

An example of such a statistical property is the *accuracy* of the
predictor $\hat Y$ defined as $\mathbb{P}\{Y=\hat Y\}.$ Many other
properties highlight different aspects of the predictor. In a binary
classification setting, for example, we can consider the conditional
probability $\mathbb{P}\{E\mid C\}$ for different settings for $E$
and $C$. 

      $E$        $C$   Resulting notion
  ------------ ------- ----------------------------
   $\hat Y=1$   $Y=1$  True positive rate, recall
   $\hat Y=0$   $Y=1$  False negative rate
   $\hat Y=1$   $Y=0$  False positive rate
   $\hat Y=0$   $Y=0$  True negative rate

  : Common classification criteria defines as $\mathbb{P}\{E\mid C\}$
  for various settings of $E$ and $C.$[]{label="table:classification-1"}

It is not important to memorize these terms. They do, however, come up
regularly in the classification setting so that the table might come in
handy. Another family of classification criteria arises from swapping
the events $E$ and $C$. 

    $E$       $C$      Resulting notion
  ------- ------------ --------------------------------------
   $Y=1$   $\hat Y=1$  Positive predictive value, precision
   $Y=0$   $\hat Y=0$  Negative predictive value

  : Additional classification criteria[]{label="table:classification-2"}

### Score functions

It often makes sense to summarize the available data in a single
real-valued variable. We will refer to such a variable as *score* or
*regressor*.

A natural score function is the expectation of the target variable $Y$
conditional on the features we have observed. This is sometimes called
the *Bayes optimal score* or *Bayes optimal regressor*. The *Bayes
optimal* score is given by $R=r(X)$ where
$r(x) = \mathbb{E}[Y\mid X=x].$ The Bayes optimal score minimizes the
squared error $\mathbb{E}(Y-R)^2$ among all score functions.

We can always turn a score function into a discrete predictor by
discretizing its values into buckets. In the case of a binary predictor
this correspond to choosing a threshold $t$ so that when the score is
above $t$ our predictor outputs $1$ and otherwise $0$. Each choice of
the threshold defines one binary predictor. Which threshold we choose
depends on how much we like the resulting predictor for the problem
we're trying to solve. If in our problem we face a high cost for false
positives, we might want to choose a higher and therefore more
conservative threshold than in other applications where false negatives
are costly.

Fortunately, the choice of a threshold and its resulting trade-off
between true positive rate and false positive rate can be neatly
visualized with the help of an *ROC curve*. 

![Example of an ROC curve. Each point on the solid curve results is
attained by thresholding the score function at some value. The dashed
line shows the trade-offs achieved by the family of trivial classifiers
that randomly accept an instance with some probability
$p\in[0,1].$[]{label="fig:roc"}](figures/roc_curve_1.svg){width="50%"}


Sensitive characteristics
-------------------------

In many prediction tasks, the features $X$ might contain or implicitly
encode sensitive characteristics of an individual. We will set aside the
letter $A$ to designate a discrete random variable that captures one or
multiple sensitive characteristics. Different settings of $A$ correspond
to different groups of the population. This notational choice is not
meant to suggest that we can cleanly partition the set of features into
two independent categories such as "neutral" and "sensitive". In fact,
in almost all scenarios sufficiently many seemingly neutral features can
often give high accuray predictions of sensitive characteristics. After
all, if we think of $A$ as the target variable in a prediction problem,
there is no reason to believe that the remaining features would not give
a non-trivial predictor for $A.$

The choice sensitive attribute will generally have profound consequences
as it decides which groups of the population we highlight, and what
conclusions we draw from our investigation. The taxonomy induced by
discretization can on its own be a source of harm if it is too coarse,
too granular, misleading, or inaccurate.

### No fairness through unawareness

Some have hoped that removing or ignoring sensitive attributes might
somehow ensure the impartiality of the resulting classifier.
Unfortunately, this practice is usually somewhere on the spectrum
between ineffective and harmful.

In a typical data set, we have many features that are slightly
correlated with the sensitive attribute. Visiting the website
pinterest.com, for example, has a small statistical correlation with
being female.[^pinterest]

[^pinterest]: As of August 2017, 58.9% of Pinterest's users in the United
States were female. See
[here](https://www.statista.com/statistics/277759/pinterest-gender-usa/)
(Retrieved 3-27-2018)

The correlation on its own is too small to predict someone's gender with
high accuracy. However, if numerous such features are available, as is
the case in a typical browsing history, the task of predicting gender
becomes feasible at high accuracy levels.

Several slightly correlated features can be used to build high accuracy
predictors for the sensitive attribute.

![ Here we see the distribution of a single feature that
differs only very slightly between the two groups. In both groups the feature
follows a normal distribution. Only the means are slightly different in each
group.
[]{label="fig:redundant"}](figures/redundant_correlation.svg "fig:"){width="48%"}

![ Multiple features of the kind plotted previously can be used to build a high accuracy
group membership predictor. Here we see how the accuracy grows as more an more
features become available.
[]{label="fig:redundant"}](figures/redundant_accuracy.svg "fig:"){width="48%"}

In large feature spaces sensitive attribute are generally *redundant*
given the other features. If a predictor trained on the original data
uses the sensitive attribute and we remove it, it will then find a
redundant encoding in terms of the other features. This results in an
essentially equivalent predictor, in the sense of implementing the same
function.

To further illustrate the issue, consider a fictitious start-up that sets
out to predict your income from your genome. At first, this task might
seem impossible. How could someone's DNA reveal their income? However,
we know that DNA accurately encodes ancestry (thus race), which in turn
correlates with income in some countries, such as, the United States.
Hence, DNA can likely be used to predict income better than random guessing. The resulting predictor uses race in an entirely implicit
manner. Removing redundant encodings of race and ancestry from the
genome is a difficult task that cannot be accomplished by removing a few
individual genetic markers. What we learn from this is that machine
learning can wind up building predictors for sensitive attributes
without explicitly being asked to, simply because it is an available
route to improving prediction accuracy.

Redundant encodings typically abound in large feature spaces. What about
small hand-curated feature spaces? In some studies, features are chosen
carefully so as to be roughly statistically independent of each other.
In such cases, the sensitive attribute may not have good redundant
encodings. That does not mean that removing it is a good idea. A medical
diagnosis, for example, often depends on race or gender in entirely
legitimate ways. Ignoring the sensitive attribute in such ases can lead
to worse predictions and resulting harm to the individual.

Formal non-discrimination criteria
----------------------------------

Many *fairness criteria* have been proposed over the years, each aiming
to formalize different desiderata. Most of these criteria are properties
of the joint distribution of the sensitive attribute $A$, the target
variable $Y$ and the predictor or score $R.$[^jointdist]

[^jointdist]: If all variables are binary, for example, then the joint
distribution is specified by 8 non-negative parameters that sum
to 1. A non-trivial property of the joint distribution would restrict
the way in which we can choose these parameters.

To a first approximation, most of these critieria fall into one of three
different categories defined along the lines of different (conditional)
independence[^condind] statements between the involved random variables.

[^condind]: Recall, random ariables $U$ and $V$ are conditionally independent given another random variable $W$ if observing $W$ makes them statistically independent. Formally, we write $U\bot V\mid W$ to indicate that 
$\mathbb{P}\{U=u,V=v\mid W=w\}$ $=\mathbb{P}\{U=u\mid W=w\}\mathbb{P}\{V=v\mid W=w\}$. Learn more [here](https://en.wikipedia.org/wiki/Conditional_independence).

   Independence      Separation        Sufficiency
  -------------- ------------------ -----------------
    $R\bot A$     $R\bot A \mid Y$   $Y\bot A\mid R$

Below we will introduce and discuss each of these conditions in detail. Variants of
these criteria arise from different ways of relaxing them. 

As an exercise, think about why we ommitted the conditional independence statement $R\bot Y\mid A$ from our discussion here.

### Independence

Our first formal criterion simply requires the sensitive characteristic
to be statistically independent of the score.

begin-Definition

The random variables $(A, R)$ satisfy *independence* if
$A\bot R.$

end-Definition

Independence has been explored through many variants and relaxations,
referred to as *demographic parity*, *statistical parity*, *group
fairness*, *disparate impact* and others. In the case of binary
prediction, independence simplifies to the condition
$$\mathbb{P}\{R=1\mid A=a\}=\mathbb{P}\{R=1\mid A=b\}\,,$$ for all
groups $a, b.$ Thinking of the event $R=1$ as "acceptance", the
condition requires the acceptance rate to be the same in all groups.
A relaxation of the constraint introduces a positive amount of slack $\epsilon>0$
and requires that
$$\mathbb{P}\{R=1\mid A=a\}\ge \mathbb{P}\{R=1\mid A=b\}-\epsilon\,.$$

Note that we can swap $a$ and $b$ to get an inequality in the
other direction.  An alternative relaxation is to consider a ratio condition, such as,
$$\frac{\mathbb{P}\{R=1\mid A=a\}}
{\mathbb{P}\{R=1\mid A=b\}}\ge1-\epsilon\,.$$ Some have
argued [@Scheidegger] that, for $\epsilon=0.2,$ this condition 
relates to the "80% rule" that we discussed in the context of disparate
impact law.

Yet another way to state the independence condition in full generality
is to require that $A$ and $R$ must have zero mutual
information[^mutualinformation] $I(A;R)=0.$ The characterization in terms of
mutual information leads to natural and interpretable relaxations of the
constraint. For example, we could require $I(A;R)\le\epsilon.$

[^mutualinformation]: Mutual information is defined as $I(A;R)=H(A)+H(R)-H(A,R),$
where $H$ denotes the entropy.

### Weakness of Independence

Although natural and pursued in many papers on the topic, independence
on its own has weaknesses.

*Laziness* describes an important problem with independence. Imagine a
company that in group $a$ hires diligently selected applicants at some
rate $p>0.$ In group $b$, the company hires carelessly selected
applicants at the same rate $p.$ Even though the acceptance rates in
both groups are identical, it is far more likely that unqualified
applicants are selected in one group than in the other. As a result, it
will appear in hindsight that members of group $b$ performed worse than
members of group $a,$ thus establishing a negative track record for
group $b.$[^selffulfilling]

[^selffulfilling]: Dwork et al.[@dwork2012fairness] called this feedback loop
*self-fulfilling prophecy*.

Laziness arises naturally in cases where the company might have
historically hired primarily employees from group $a,$ giving them a
better understanding of this group. This is also a technical matter. The
company might have substantially more training data in group $a,$ thus
potentially leading to lower error rates of a learned predictor within
that group. The last point is a bit subtle. After all, if both groups
were entirely homogenous in all ways relevant to the classification
task, more training data in one group would equally benefit both. Then
again, the mere fact that chose to distinguish these two groups
indicates that we belive they are heterogenous in relevant aspects.

### Achieving independence

There are numerous natural ways of achieving the independence criterion
depending on the class of predictors and the problem at hand. We generally distinguish between three different techniques:

* Pre-processing
* Post-processing
* At training time

Post-processing refers to the process of taking a trained predictor and
adjusting it possibly depending on the sensitive attribute and
additional randomness in such a way that independence is achieved. Formally, we say a *derived predictor* $\hat Y = F(R, A)$ is a possibly
randomized function of a given score $R$ and the sensitive attribute.
Given a cost for false negatives and false positives, we can easily find
the derived predictor that minimizes the given cost function. Post-processing has the advantage of being simple and efficient, but it can easily lead to a significant loss in utility.

Pre-processing is a family of techniques to transform a feature space
into a representation that as a whole is independent of the sensitive
attribute. 

At training time, we can work the independence constraint into the
optimization problem that we solve.

### Separation

Our next criterion acknowledges that in many scenarios the sensitive
characteristic may be correlated with the target variable. It is thus
natural to allow some correlation between the score and the sensitive
attribute to the extent that it is "justified by the target variable".
This intuition can be made precise with a simple conditional
independence statement.

begin-Definition

A random variable $R$ is *separated from $A$* if
$R\bot A \mid Y.$

end-Definition

[^separationgm]

[^separationgm]:
  {-} We can display separation as a graphical model in which $R$ is separated
from $A$ by the target variable $Y$:  
![](figures/R-Y-A.svg){width="75%"}  
If you haven't seen graphical models before, feel free to ignore this
interpretation.

In the case where $R$ is a binary classifier $C$, separation is
equivalent to requiring for all groups $a,b$ the two constraints
$$\begin{aligned}
\mathbb{P}\{ C=1 \mid Y=1, A=a\} &= \mathbb{P}\{ C=1 \mid Y=1, A=b\}\\
\mathbb{P}\{ C=1 \mid Y=0, A=a\} &= \mathbb{P}\{ C=1 \mid Y=0, A=b\}\,.\end{aligned}$$

Recall that $\mathbb{P}\{ C=1 \mid Y=1\}$ is called the *true positive
rate* of the classifier. It is the rate at which the classifier
correctly recognizes positive instances. The *false positive rate*
$\mathbb{P}\{ C=1 \mid Y=0\}$ highlights the rate at which the
classifier mistakenly assigns positive outcomes to negative instances.
What separation therefore requires is that all groups experience the
same false negative rate and the same false positive rate.

This interpretation in terms of equality of error rates leads to natural
relaxations. For example, we could only require equality of false
negative rates. A false negative, intuitively speaking, corresponds to
denied opportunity in cases where acceptance is desirable.

### Achieving separation

As was the case with independence, we can achieve separation by
post-processing a given score function without the need for
retraining.[^derived]

[^derived]: Recall, a derived predictor is a possible randomized mapping
$\hat Y=F(R,A).$

A binary predictor that satisfies separation must achieve the same true positive
rates and the same false positive rates in all groups.  This condition
corresponds to taking the intersection of all group-level ROC curves. Within
this constraint region, we can then choose the predictor that minimizes the
given cost.

![Here we have the ROC curves of a score displayed for each
group separately. The two groups have different curves.
[]{label="fig:roc-overlap"}](figures/roc_curve_2.svg "fig:"){width="48%"}

![The highlighted region corresponds to
the intersection of the area below each ROC
curve.[]{label="fig:roc-overlap"}](figures/roc_curve_3.svg "fig:"){width="48%"}

### Sufficiency

Our third criterion formalizes that the score already subsumes the
sensitive characteristic for the purpose of predicting the target. This
idea again boils down to a natural conditional independence statement.

begin-Definition

A random variable $R$ is *sufficient for $A$* if
$Y\bot A \mid R.$

end-Definition

[^sufficiencygm]

[^sufficiencygm]:
  {-} We can again display sufficiency as a graphical model as we did with
separation before:  
![](figures/Y-R-A.svg){width="75%"}  
If you haven't seen graphical models before, feel free to ignore this
interpretation.

We will often just say that $R$ satisfies *sufficiency* when $A$ is
clear from the context.

Let us write out the definition more explictly in the binary case where
$Y\in\{0,1\}.$ In this case, a random variable $R$ is sufficient for $A$
if and only if for all groups $a,b$ and all values $r$ in the support
of $R,$ we have
$$\mathbb{P}\{Y=1 \mid R=r, A=a\}=\mathbb{P}\{ Y=1 \mid R=r, A=b\}\,.$$
When $R$ has only two values we recognize this condition as requiring a
parity of positive/negative predictive values across all groups.

While it is often useful to think of sufficiency in terms of positive and negative predictive values, there's a more intuitive alternative. Indeed,
sufficiency turns out to be closely related to an important notion called *calibration*, as we will discuss next.

Calibration and sufficiency
---------------------------

In some applications it is desirable to be able to interpret the values
of the score functions as probabilities. Formally, we say that a
score $R$ is *calibrated* if for all score values $r$ in the support
of $R,$ we have

$$\mathbb{P}\{Y=1 \mid R=r\} = r\,.$$

This condition means that the set of all instances assigned a score
value $r$ has an $r$ fraction of positive instances among them. This
does not mean that at at the level of an individual instance a score of
$r$ means that this instance has a probability $r$ of seeing a positive
outcome.

The Bayes optimal score certainly satisfies this as the score is indeed
the probability of a positive outcome given the observed covariates. In
fact, the Bayes optimal score remains calibrated when restricted to any
subpopulation even if it constists of only a single instance.

In practice, there are various methods to achieve calibration. *Platt
scaling* is a popular method that works as follows.

Given an uncalibrated score $R,$ Platt scaling aims to find scalar
parameters $a, b$ such that the sigmoid function[^sigmoid]
$$S = \frac1{1+\exp(a R+b)}$$ fits the target variable $Y$ with respect
to the so-called *log loss* $$-\mathbb{E}[Y\log S + (1-Y)\log(1-S)].$$
This objective can be minimized given labeled examples drawn from
$(R, Y)$ as is standard in supervised learning.

[^sigmoid]: {-} A plot of the sigmoid function $1/(1+\exp(-x)).$  
![logit function](figures/logit_function.svg){width="75%"}

### Calibration by group

From the equations above, we can also see that sufficiency is closely
related to the idea of calibration. To formalize the connection we say
that the score $R$ satisfies *calibration by group* if it satisfies
$$\mathbb{P}\{Y=1 \mid R=r, A=a\} = r\,,$$ for all score values $r$ and
groups $a.$ Recall, that calibration is the same requirement at the
population level without the conditioning on $A.$

begin-Fact

Calibration by group implies sufficiency.

end-Fact

Conversely, sufficiency is only slightly weaker than calibration by
group in the sense that a simple renaming of score values goes from one
property to the other.

begin-Proposition

If a score $R$ satisfies sufficiency, then there exists a
function $\ell\colon[0,1]\to[0,1]$ so that $\ell(R)$ satisfies
calibration by group.

end-Proposition

begin+Proof

Fix any group $a$ and put
$\ell(r) = \mathbb{P}\{Y=1\mid R=r, A=a\}.$ Since $R$ satisfies
sufficiency, this probability is the same for all groups $a$ and hence
this map $\ell$ is the same regadless of what value $a$ we chose.

Now, consider any two groups $a,b.$ We have, $$\begin{aligned}
r 
&= \mathbb{P}\{Y=1\mid \ell(R)=r, A=a\} \\
&= \mathbb{P}\{Y=1\mid R\in \ell^{-1}(r), A=a\} \\
&= \mathbb{P}\{Y=1\mid R\in \ell^{-1}(r), A=b\} \\
&= \mathbb{P}\{Y=1\mid \ell(R)=r, A=b\}\,,\end{aligned}$$ thus showing
that $\ell(R)$ is calibrated by group.

end-Proof

We conclude that sufficiency and calibration by group are essentially
equivalent notions. In particular, this gives us a large repertoire of
methods for achieving sufficiency. We could, for example, apply Platt
scaling for each of the groups defined by the sensitive attribute. 

### Calibration as a consequence of unconstrained learning

Sufficiency is often satisfied by default without the need for any
explicit invervention. Indeed, we generally expect a learned score to
satisfy sufficiency in cases where the sensitive attribute can be
predicted from the other attributes.

To illustrate this point we look at the calibration values of a standard
logistic regression model on the standard UCI adult data set.[^uciadult]

[^uciadult]: 
  [Source](https://archive.ics.uci.edu/ml/datasets/adult)

We fit a logistic regression model using Python's sklearn library on the
UCI training data. The model is then applied to the UCI test data. We
make no effort to either tune or calibrate the model.

As we can see from the figure below, the model turns out to be well calibrated
by group on its own. The deviation we see in the mid deciles on the left
hand side is likely due to the scarcity of the test data in the
corresponding group and deciles. For example, the $6$th decile,
corresponding to the score range $(0.5, 0.6],$ on the test data has only
$34$ instances with the 'Race' attribute set to 'Black'.

![ 
[]{label="fig:adult-calibration"}](figures/adult_calibration_race.svg "fig:"){width="48%"}
![ Figures show how well a standard logistic regression model is
calibrated by group. A straight diagonal line would be perfect
calibration. Number of test samples: 1561 Black, 13946 White; 5421
Female, 10860
(Male)[]{label="fig:adult-calibration"}](figures/adult_calibration_gender.svg "fig:"){width="48%"}
[^numsamplesadult]

These figures show how well a standard logistic regression model is
calibrated by group. A straight diagonal line would be perfect
calibration.

[^numsamplesadult]: 
  {-} Number of test samples: 1561 Black, 13946 White; 5421 Female, 10860 Male

The lesson is that calibration often comes for free as a consequence of
standard machine learning practices. The flip side is that imposing
sufficiency as a constraint on a classification system may not be much
of an intervention. In particular, it would not effect a substantial
change in current practices.

What is the purpose of a fairness criterion?
--------------------------------------------

The independence criterion highlights the necessity to distinguish
between a belief about human nature, a long-term goal for society, and a
near-term intervention. Some might defend independence based on the
belief that *we're all created equal* and hence certain intrinsic human
features should be *independent* (in the informal sense) of an attribute
such as race or gender. Others might defend independence based on the
desire to live in a society where the sensitive attribute is independent
(even in the formal sense) of features such as income or level of
education. Neither of these arguments, however, address the question
whether independence is a useful constraint that we should impose an a
classification system.

In a lending setting, for example, independence would result in the same
rate of lending in all demographic groups defined by the sensitive
attribute. If the latter refers to race, this practice would ignore the
reality that some financial indicators, such as net worth or income, may
be unevenly distributed in different racial groups. Lenders currently
give out loans at different rates to different groups and this may be
acceptable if it reflects differing repay abilities of those groups.
Giving out loans to individuals who cannot pay it back does not only
diminish profit for the bank, it also impoverishes the individual who
defaults.


Trade-offs between criteria
---------------------------

The criteria we reviewed constrain the joint distribution of the random
variables $(R, A, Y)$ in non-trivial ways. Therefore, we should suspct
that imposing any two of them simultaneously over-constrains the space
to the point where only few solutions remain. We will now see that this
intuition is largely correct.

In essence, these results show that we cannot impose multiple criteria
as hard constraints. Rather we need to strike trade-offs between them.

### Independence versus Sufficiency

We begin with a simple proposition that shows how in general
independence and sufficiency are mutually exclusive. The only assumption
needed here is that the sensitive attribute $A$ and the target variable
$Y$ are *not* independent. This is a different way of saying that group
membership has an effect on the statistics of the outcome variable. For
instance, one group might have a higher rate of positive outcomes than
another. Think of this as the typical case.

begin-Proposition

Assume that $A$ and $Y$ are not independent. Then
sufficiency and independence cannot both hold.

end-Proposition

begin+Proof

By the contraction rule for conditional independence,

$$A\bot R \quad\mathrm{and}\quad A\bot Y \mid R
\quad\Longrightarrow\quad
A\bot Y, R
\quad\Longrightarrow\quad
A\bot Y\,.$$ 

In the contrapositive, $$A\not\bot Y 
\quad\Longrightarrow\quad
A\not\bot R
\quad\mathrm{or}\quad
A\not\bot R \mid Y\,.$$ 

end-Proof

### Independence versus Separation

We can prove an analogous result of mutual exclusion for sufficiency and
separation. In this case, we additionally need the assumption that the
target variable $Y$ is binary. This is with loss of generality, since
the case of more than two outcomes is certainly worth entertaining. The
exploration of this case is beyond the scope of this chapter.

begin-Proposition

Assume that $Y$ is binary and that $A$ and $Y$ are not
independent. Then, independence and separation cannot both hold.

end-Proposition

begin+Proof

end-Proof

### Separation versus Sufficiency

Finally, we turn to the relationship between separation and sufficiency.
Both ask for a non-trival conditional independence relationship between
the three variable $A, R, Y.$ We should therefore expect that imposing
both simultaneously leads to a degenerate solution space. The next
proposition confirms our suspicion.

begin-Proposition

Assume that all events in the joint distribution of
$(A, R, Y)$ have positive probability, and assume $A\not\bot Y.$ Then,
separation and sufficiency cannot both hold.

end-Proposition

begin+Proof

A standard fact[^wasserman] about conditional independence shows

[^wasserman]: See Theorem 17.2 in [@wasserman2010all]

$$A\bot R \mid Y\quad\text{and}\quad A\bot Y\mid R
\quad\implies\quad A\bot (R, Y)\,.$$ Moreover,
$$A\bot (R,Y)\quad\implies\quad A\bot R\quad\text{and}\quad A\bot Y\,.$$
Taking the contrapositive completes the proof. 

end-Proof

For a binary target, the non-degeneracy assumption in the previous
proposition states that in all groups, at all score values, we have both
positive and negative instances. In other words, the score value never
fully resolves uncertainty regarding the outcome.

In case the predictor is also binary, we can weaken the assumption to
require only that the predictor is imperfect in the sense of making at
least one false positive predicion.

begin-Proposition

Assume $Y$ is not independent of $A$ and assume $\hat Y$ is
a binary predictor with nonzero false positive rate. Then, separation
and sufficiency cannot both hold.

end-Proposition

begin+Proof

Since $Y$ is not independent of $A$ there must be two groups,
say, $0$ and $1$, such that
$$p_0=\mathbb{P}\{Y=1\mid A=0\}\ne \mathbb{P}\{Y=1\mid A=1\}=p_1\,.$$
Now suppose that separation holds. Since the predictor is imperfect this
means that all groups have the same non-zero false positive rate
$\mathrm{FPR}>0,$ and the same positive true positive rate
$\mathrm{TPR}>0.$ We will show that sufficiency holds.

Recall that in the binary case, sufficiency implies that all groups have
the same positive predictive value. The positive predictive value in
group $a,$ denoted $\mathrm{PPV}_a$ satisfies $$\mathrm{PPV_a} 
= \frac{\mathrm{TPR}p_a}{\mathrm{TPR}p_a+\mathrm{FPR}(1-p_a)}\,.$$ From
the expression we can see that $\mathrm{PPV}_0=\mathrm{PPV}_1$ only if
either $\mathrm{TPR}=0$ or $\mathrm{FPR}=0.$ The latter is ruled out by
assumption. So it must be that $\mathrm{TPR}=0.$ However, in this case,
we can verify that the negative predictive value $\mathrm{NPV}_0$ in
group $0$ must be different from the negative predictive
value $\mathrm{NPV}_1$ in group $1.$ This follows from the expression
$$\mathrm{NPV_a} 
= \frac{(1-\mathrm{FPR})(1-p_a)}{(1-\mathrm{TPR})p_a+(1-\mathrm{FPR})(1-p_a)}\,.$$
Hence, sufficiency does not hold. 

end-Proof

Exercise. Prove the following result: Assume $Y$ is not independent
of $A$ and assume $\hat Y$ is a binary predictor with nonzero false
positive rate and nonzero true positive rate. Then, if separation holds,
there must be two groups with different positive predictive values.

Inherent limitations of observational criteria
----------------------------------------------

All the criteria we've seen so far have one striking aspect in common.
They are all properties of the joint distribution of the score,
sensitive attribute, and the outcome. In other words, if we know the
joint distribution of the random variables $(R, A, Y),$ we can without
ambiguity determine whether this joint distribution satisfies one of
these criteria or not.[^binary]

[^binary]: {-} For example, if all variables are binary, there are $8$
numbers specifying the joint distributions. We can verify the property
by looking only at these $8$ numbers.

We can broaden this notion a bit and include also all features including
the sensitive attribute.

We call a definition *observational* if it is a property of the joint
distribution of the features $X,$ the sensitive attribute $A,$ a score
function $R$ and an outcome variable $Y.$[^observational]

[^observational]: Formally, this means an observational property is defined by
set of joint distributions over a given set of variables.

Convince yourself that independence, separation, and sufficiency are all
observational definitions.

Observational definitions have many appealing aspects, but they also
share inherent limiations, as we will see next.

In Scenario I we\...

![image](figures/scenario1-gm.svg){width="50%"} 

Graphical model for Scenario I.

In Scenario II we \...

![image](figures/scenario2-gm.svg){width="50%"} 

Graphical model for Scenario II.

begin-Proposition

The random variables in Scenario I and II admit identical
joint distributions. In particular, no observational criterion
distinguishes between the two scenarios.

end-Proposition

### What is not observational?

At this point, it might seem hard to conceive of a definition that is
not observational. This is in part, because we are so used to thinking
in terms of observational criteria.

Case study: Recidivism prediction and criminal justice
------------------------------------------------------

Bibliographic notes and further reading
---------------------------------------

Who first proposed independence as a fairness criterion is not exactly
clear. However, some early works[@...] come to mind. Numerous works have
since dealt with variants of independence in different settings. Zemel
et al.[@zemel2013learning] adopted the mutual information viewpoint and
proposed an heuristic pre-processing approach for minimizing mutual
information. Dwork et al.[@dwork2012fairness] already criticized the
independence criterion, pointing out some of the issues with the notion.

The separation criterion was proposed by Hardt et
al.[@hardt2016equality], alongside the relaxation to equal false
negative rates, which they called *equality of opportunity.* These
criteria also appeared in an independent work[@zafar2017fairnessbeyond].
ProPublica[@angwin2016machine] implicitly adopted equality of false
positive rates as a fairness criterion in their article on COMPAS
scores. Northpointe, the maker of the COMPAS software, emphasized the
importance of calibration by group in their rebuttal of ProPublic's
article. Similar arguments were made quickly after the publication of
ProPublica's article by bloggers including Abe Gong.

Variants of the trade-off between separation and sufficiency were shown
by Chouldechova[@chouldechova2016fair] and Kleinberg et
al.[@kleinberg2016inherent]. Subsequent work[@weinberger2017calibration]
considers trade-offs between relaxed and approximate criteria. The other
trade-off results presented in this chapter are new to this book.
